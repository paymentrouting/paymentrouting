package com.adesso.swift;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.prowidesoftware.swift.model.MtSwiftMessage;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import com.prowidesoftware.swift.model.mt.mt1xx.MT103;
import com.prowidesoftware.swift.model.mt.mt2xx.MT202;
import com.prowidesoftware.swift.model.mt.mt2xx.MT202COV;
import com.prowidesoftware.swift.model.mt.mt7xx.MT742;
import com.prowidesoftware.swift.model.mt.mt9xx.MT910;
import com.semedy.reasoner.api.reasoning.ReasoningException;
import com.semedy.reasoner.api.reasoning.SemReasonerException;

public class SWIFTReader {

	/**
	 * reads all SWIFT messages from a directory and returns them Generates unique
	 * ids for each message
	 * 
	 * @param path,
	 *            path of directory containing SWIFT messages
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ReasoningException
	 * @throws SemReasonerException
	 */
	public List<JSONObject> read(String path) throws IOException {
		List<JSONObject> result = new ArrayList<>();
		File dir = new File(path);
		if (dir.isDirectory()) {
			File[] files = dir.listFiles();
			for (int i = 0; i < files.length; i++) {
				JSONObject json = null;
				File fin = files[i];
				MtSwiftMessage msg = MtSwiftMessage.parse(fin);
				String messagetype = msg.getMessageType();
				AbstractMT special = null;
				String type = "";
				if (messagetype.equals("202")) {
					special = MT202.parse(fin);
					type = "MT202";
				} else if (messagetype.equals("103")) {
					special = MT103.parse(fin);
					type = "MT103";
				} else if (messagetype.equals("910")) {
					special = MT910.parse(fin);
					type = "MT910";
				} else if (messagetype.equals("742")) {
					special = MT742.parse(fin);
					type = "MT742";
					
				} else if (messagetype.equals("202COV")) {
					special = MT202COV.parse(fin);
					type = "MT202COV";
				}
				if (special != null) {	
					json = new JSONObject(special.json());
			
					JSONObject resultJson = semantize(flattenJson(json),type,i);
					// System.out.println(resultJson.toString(4));
					result.add(resultJson);
				}
			}
		}
		return result;
	}

	private void flattenJsonObject(JSONObject source, JSONObject result) {
		for (String prop : JSONObject.getNames(source)) {
			Object o = source.get(prop);
			if (!(o instanceof JSONObject) && !(o instanceof JSONArray))
				result.put(prop, o);
		}
	}

	private Object[] swift32a(String swift32a) {
		int i = 0;
		while (swift32a.charAt(i) >= '0' && swift32a.charAt(i) <= '9')
			i++;
		String valueDate = swift32a.substring(0, i);
		String year = valueDate.substring(0, 2);
		String month = valueDate.substring(2, 4);
		String day = valueDate.substring(4);
		GregorianCalendar greg = new GregorianCalendar(Integer.parseInt(year), Integer.parseInt(month),
				Integer.parseInt(day));
		com.semedy.reasoner.symboltable.encoder.Calendar calendar = new com.semedy.reasoner.symboltable.encoder.Calendar(
				greg);
		String currency = swift32a.substring(i, i + 3);
		String value = swift32a.substring(i + 3);

		Object[] result = new Object[3];
		result[0] = calendar.getString();
		result[1] = currency;
		result[2] = Float.parseFloat(value.replace(',', '.'));
		return result;
	}

	private JSONObject flattenJson(JSONObject json) {
		JSONObject result = new JSONObject();
		int block = 1;
		flattenJsonObject(json, result);
		JSONObject jsondata = json.getJSONObject("data");
		while (jsondata.has("block" + block)) {
			Object o = jsondata.get("block" + block);
			if (o instanceof JSONObject)
				flattenJsonObject((JSONObject) o, result);
			else if (o instanceof JSONArray)
				for (int i = 0; i < ((JSONArray) o).length(); i++)
					flattenJsonObject(((JSONArray) o).getJSONObject(i), result);
			block++;
		}
		
		return result;
	}
	
	private JSONObject semantize(JSONObject json, String type, int instanceNumber) {
		if (json.has("20")) {
			json.put("sendersReference",json.get("20"));
			json.remove("20");
		}
		if (json.has("21")) {
			json.put("referenceToRelatedTransaction",json.get("21"));
			json.remove("21");
		}
		if (json.has("32A")) {
			Object[] swift32a = swift32a(json.getString("32A"));
			json.put("value", swift32a[2]);
			json.put("currency", swift32a[1]);
			json.put("valueDate", swift32a[0]);
			json.remove("32A");
		}
		json.put("@type", type);
		json.put("@id", "" + instanceNumber);
		return json;
		
	}
}
