package com.adesso.swift;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.semedy.reasoner.api.builtin.BuiltinProvider;
import com.semedy.reasoner.api.core.Configuration;
import com.semedy.reasoner.api.core.Configuration.StorageType;
import com.semedy.reasoner.api.core.Core;
import com.semedy.reasoner.api.core.DeductiveDatabase;
import com.semedy.reasoner.api.core.JSONDeductiveDatabase;
import com.semedy.reasoner.api.core.ResultBuffer;
import com.semedy.reasoner.api.core.ResultEnumerator;
import com.semedy.reasoner.api.edb.EDBException;
import com.semedy.reasoner.api.idb.IntensionalDB;
import com.semedy.reasoner.api.logic.Literal;
import com.semedy.reasoner.api.logic.Rule;
import com.semedy.reasoner.api.parser.SimpleParseException;
import com.semedy.reasoner.api.reasoning.InterruptFlag;
import com.semedy.reasoner.api.reasoning.ReasoningException;
import com.semedy.reasoner.api.reasoning.ReasoningMonitor;
import com.semedy.reasoner.api.reasoning.SemReasonerException;
import com.semedy.reasoner.api.symboltable.SymbolTable;
import com.semedy.reasoner.api.symboltable.SymboltableException;
import com.semedy.reasoner.reasoning.processes.ReasoningRelation;
import com.semedy.reasoner.symboltable.encoder.JsonIdentifier;

public class KnowledgeModelImp implements KnowledgeModel {
	/** the central reasoning core, also used as message pool */
	private Core _core;
	/** the configuration */
	private Configuration _config;

	public KnowledgeModelImp()
			throws SemReasonerException, IOException, EDBException, SymboltableException, InterruptedException {
		_config = new Configuration();
		_config.load();
		_core = new Core(_config);
		loadImportDirectory();
	}

	private void select(File f, List<String> predfiles, List<String> rawfiles, List<String> jsonfiles) {
		String path = f.getAbsolutePath();
		if (path.endsWith("fct"))
			predfiles.add(f.getAbsolutePath());
		else if (path.endsWith("rls"))
			predfiles.add(f.getAbsolutePath());
		else if (path.endsWith("json"))
			jsonfiles.add(f.getAbsolutePath());
		else if (path.endsWith("raw"))
			rawfiles.add(f.getAbsolutePath());
	}

	/*
	 * load initial data and rule files
	 */
	private void loadImportDirectory() throws EDBException, IOException, SimpleParseException, SemReasonerException,
			InterruptedException, SymboltableException {
		List<String> predfiles = new ArrayList<>();
		List<String> rawfiles = new ArrayList<>();
		List<String> jsonfiles = new ArrayList<>();
		File f = new File(_config.getImportDir());
		if (f.isDirectory()) {
			File[] files = f.listFiles();
			for (int i = 0; i < files.length; i++)
				select(files[i], predfiles, rawfiles, jsonfiles);
		} else
			select(f, predfiles, rawfiles, jsonfiles);
		if (predfiles.size() + rawfiles.size() > 0) {
			DeductiveDatabase ddb = new DeductiveDatabase(_core);
			if (predfiles.size() > 0)
				ddb.loadPredicateFiles(predfiles);
			if (rawfiles.size() > 0)
				ddb.loadRawFiles(rawfiles, false);
		}
		if (jsonfiles.size() > 0) {
			JSONDeductiveDatabase jdb = new JSONDeductiveDatabase(_core);
			jdb.loadJson(jsonfiles);
		}
	}

	@Override
	public boolean addMessage(JSONObject message) throws SemReasonerException, EDBException, IOException {

		JSONDeductiveDatabase jddb = new JSONDeductiveDatabase(_core);
		boolean result = false;
		try {
			jddb.transactionBegin();
			result = jddb.addJsonObject(message);
			jddb.transactionCommit();
		} catch (SemReasonerException | IOException | ReasoningException | InterruptedException e) {
			try {
				jddb.transactionRollback();
			} catch (SemReasonerException | IOException e1) {
				throw new SemReasonerException(e.getMessage());
			}
		}
		return result;
	}

	@Override
	public boolean removeMessage(String id) throws EDBException, IOException, SemReasonerException {
		JSONDeductiveDatabase jddb = new JSONDeductiveDatabase(_core);
		boolean result = false;
		try {
			jddb.transactionBegin();
			result = jddb.removeJsonObject(id);
			jddb.transactionCommit();
		} catch (SemReasonerException | IOException | ReasoningException | InterruptedException e) {
			try {
				jddb.transactionRollback();
			} catch (SemReasonerException | IOException e1) {
				throw new SemReasonerException(e.getMessage());
			}
		}
		return result;
	}

	@Override
	public JSONObject getMessage(String id)
			throws ReasoningException, InterruptedException, IOException, SemReasonerException {
		JSONDeductiveDatabase jddb = new JSONDeductiveDatabase(_core);
		JSONObject result = jddb.retrieveJsonObject(id);
		return result;
	}

	@Override
	public List<JSONObject> match() throws IOException, ReasoningException, InterruptedException, SemReasonerException {
		return createMatchResult("?- match(?X,?Y,?R).");

	}

	private List<JSONObject> createMatchResult(String query)
			throws IOException, ReasoningException, InterruptedException, SemReasonerException {
		DeductiveDatabase ddb = new DeductiveDatabase(_core);
		Map<JsonIdentifier, List<JsonIdentifier>> matches = new HashMap<>();
		try (ResultEnumerator enm = ddb.query(new InterruptFlag(), query, -1, null)) {
			enm.nextElement();
			ResultBuffer buf = enm.getBuffer();
			while (enm.hasMoreElements()) {
				enm.nextElement();
				JsonIdentifier messageId = (JsonIdentifier) buf.get("?X");
				List<JsonIdentifier> matchList = matches.get(messageId);
				if (matchList == null) {
					matchList = new ArrayList<>();
					matches.put(messageId, matchList);
				}
				matchList.add((JsonIdentifier) buf.get("?Y"));
			}
		}
		JSONDeductiveDatabase jddb = new JSONDeductiveDatabase(_core);
		List<JSONObject> result = new ArrayList<>();
		for (Entry<JsonIdentifier, List<JsonIdentifier>> E : matches.entrySet()) {
			JSONObject json = new JSONObject();
			JsonIdentifier jsonid = (JsonIdentifier) E.getKey();
			String s = jsonid.getValue().toString();
			JSONObject message = jddb.retrieveJsonObject(s);
			JSONArray array = new JSONArray();
			array.put(message);
			json.put("messages", array);
			List<String> matchids = new ArrayList<>();
			for (JsonIdentifier id : E.getValue())
				matchids.add(id.getValue().toString());
			List<JSONObject> matched = jddb.retrieveJsonObjects(matchids);
			json.put("matchTo", matched);
			result.add(json);
		}
		return result;
	}

	@Override
	public List<JSONObject> caseBasedMatch()
			throws IOException, ReasoningException, InterruptedException, SemReasonerException {
		return createMatchResult("?- caseBasedMatch(?X,?Y,?Z).");
	}

	@Override
	public boolean addToCaseBase(JSONObject matchBeforeFixes, JSONObject matchAfterFixes)
			throws IOException, SemReasonerException, JSONException, ReasoningException, InterruptedException {
		Configuration config = new Configuration();
		config.load();
		config.setEDBStorageType(StorageType.MEMORY);
		config.setSymbolTableInMainMemory(true);
		try (Core core = new Core(config)) {
			JSONDeductiveDatabase jddb = new JSONDeductiveDatabase(core);
			core.getIntensionalDB().addAllRules(_core.getIntensionalDB().getRules());
			jddb.transactionBegin();
			jddb.addJsonObjects(matchAfterFixes.getJSONArray("messages"), "");
			jddb.addJsonObjects(matchAfterFixes.getJSONArray("matchTo"), "");

			DeductiveDatabase ddb = new DeductiveDatabase(_core);
			String ruleIdentifier = null;
			try (ResultEnumerator enm = ddb.query(new InterruptFlag(), "?- match(?X,?Y,?Z).", -1, null)) {
				enm.nextElement();
				ResultBuffer buf = enm.getBuffer();
				if (!enm.hasMoreElements())
					return false;
				enm.nextElement();
				ruleIdentifier = buf.get("?Z").toString();
			}
			jddb.transactionRollback();

			jddb.transactionBegin();
			jddb.addJsonObjects(matchBeforeFixes.getJSONArray("messages"), "");
			jddb.addJsonObjects(matchBeforeFixes.getJSONArray("matchTo"), "");
			LearnMonitor monitor = new LearnMonitor(ruleIdentifier);
			Literal firstFalseLiteral = null;
			do {
				try (ResultEnumerator enm = ddb.query(new InterruptFlag(), "?- match(?X,?Y,?Z).", -1, monitor)) {
					enm.nextElement();
					if (!enm.hasMoreElements()) {
						// relax rule
						Rule rule = core.getIntensionalDB().getRuleById(ruleIdentifier);
						firstFalseLiteral = monitor.getFirstFalseLiteral();
						if (firstFalseLiteral != null) {
							int index = -1;
							for (int i = 0; i < rule.getBodyLength(); i++)
								if (rule.getBody(i) == firstFalseLiteral)
									index = i;
							if (index != -1) {
								Literal[] bodies = new Literal[rule.getBodyLength() - 1];
								for (int i = 0; i < index; i++)
									bodies[i] = rule.getBody(i);
								for (int i = index + 1; i < rule.getBodyLength(); i++)
									bodies[i - 1] = rule.getBody(i);
								Rule newrule = new Rule(ruleIdentifier + "_" + index, rule.getHead(), bodies);
								core.getIntensionalDB().removeRuleById(ruleIdentifier);
								core.getIntensionalDB().addRule(newrule);
								ruleIdentifier = ruleIdentifier + "_" + index;
								monitor.setRuleIdentifier(ruleIdentifier);
							}
						}
					}
				}
			} while (firstFalseLiteral != null);
		}
		return true;
	}

	@Override
	public boolean hasMatchableElements() {
		return false;
	}

	@Override
	public List<JSONObject> queryForMessages(String ooquery)
			throws IOException, ReasoningException, InterruptedException, SemReasonerException {
		JSONDeductiveDatabase jddb = new JSONDeductiveDatabase(_core);
		return jddb.retrieveJsonObjects(ooquery);
	}

	@Override
	public List<JSONObject> queryForMessagesInClass(String messageClass)
			throws IOException, ReasoningException, InterruptedException, SemReasonerException {
		JSONDeductiveDatabase jddb = new JSONDeductiveDatabase(_core);
		return jddb.retrieveJsonObjects("?- ?Id:" + messageClass);
	}

	class LearnMonitor implements ReasoningMonitor {
		private Literal _firstLiteral = null;
		private String _ruleId;

		public LearnMonitor(String ruleId) {
			_ruleId = ruleId;
		}

		@Override
		public void postBuiltinExecution(Rule arg0, Literal arg1, String arg2, String arg3, ReasoningRelation[] arg4,
				long arg5, ReasoningRelation arg6, String arg7) throws IOException {
			// TODO Auto-generated method stub

		}

		@Override
		public void postEvaluation(String arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void postJoin(Rule rule, Literal literal, Literal arg2, String arg3, String arg4,
				ReasoningRelation[] arg5, ReasoningRelation[] arg6, long numberOfResults, ReasoningRelation arg8,
				String arg9, String arg10) throws IOException {
			if (numberOfResults == 0 && rule.getIdentifier().equals(_ruleId) && _firstLiteral == null)
				_firstLiteral = literal;
		}

		@Override
		public void postRuleEvaluation(Rule arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void preBuiltinExecution(Rule arg0, Literal arg1, String arg2, String arg3, ReasoningRelation[] arg4,
				String arg5) throws IOException {
			// TODO Auto-generated method stub

		}

		@Override
		public void preEvalution(String arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void preFreeMemory(String arg0, long arg1) {
			// TODO Auto-generated method stub

		}

		@Override
		public void preJoin(Rule arg0, Literal arg1, String arg2, String arg3, ReasoningRelation[] arg4,
				ReasoningRelation[] arg5, String arg6) throws IOException {
			// TODO Auto-generated method stub

		}

		@Override
		public void preMemoryAllocation(String arg0, long arg1) {
			// TODO Auto-generated method stub

		}

		@Override
		public void preRuleEvaluation(Rule arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void setBuiltins(BuiltinProvider arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void setIntensionalDB(IntensionalDB arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void setReasoningSymbolTable(SymbolTable arg0) {
			// TODO Auto-generated method stub

		}

		public Literal getFirstFalseLiteral() {
			return _firstLiteral;
		}

		public void clear() {
			_firstLiteral = null;
		}
		
		public void setRuleIdentifier(String ruleId) {
			_ruleId = ruleId;
		}

	}

}
