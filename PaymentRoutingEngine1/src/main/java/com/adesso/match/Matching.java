package com.adesso.match;

import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.semedy.reasoner.api.edb.EDBException;
import com.semedy.reasoner.api.idb.IDBException;
import com.semedy.reasoner.api.parser.SimpleParseException;
import com.semedy.reasoner.api.reasoning.ReasoningException;
import com.semedy.reasoner.api.reasoning.SemReasonerException;

/**
 * The knowledge model contains the messages and <br>
 * the rules which define the matches. <br>
 * It also has a case database containing previous matches <br>
 * to help in creating new matches based on old ones.
 * @author angele
 *
 */
public interface Matching {
	
	/**
	 * Adds a new message to the message pool. <br>
	 * The top class is Message. <br>
	 * Returns whether message has been added.<br>
	 * @param message, the message
	 * @return generated unique id of the message
	 * @throws SemReasonerException 
	 * @throws IOException 
	 * @throws EDBException 
	 */
	boolean addMessage(JSONObject message) throws SemReasonerException, EDBException, IOException;
	
	/**
	 * Removes a message from the message pool. <br>
	 * id is the unique id generated during adding. <br>
	 * @param id, the unique id of the message
	 * @return true if message has been removed, false otherwise
	 * @throws IOException 
	 * @throws EDBException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws SemReasonerException 
	 */
	boolean removeMessage(String id) throws EDBException, IOException, InstantiationException, IllegalAccessException, SemReasonerException;
	
	/**
	 * Returns a message given by the unique id <br>
	 * from the message pool.
	 * @param id, unique id of the message
	 * @return message with given id if available, null otherwise
	 * @throws SemReasonerException 
	 * @throws IOException 
	 * @throws InterruptedException 
	 * @throws ReasoningException 
	 */
	JSONObject getMessage(String id) throws ReasoningException, InterruptedException, IOException, SemReasonerException;	
	
	/**
	 * Returns a set of  (possible) matches. <br>
	 * Returns an empty list if no  match is possible. <br>
	 * With each match it is indicated whether this match is complete <br>
	 * or has to be completed by a human.
	 * @return list of matches
	 * @throws IOException 
	 * @throws EDBException 
	 * @throws InterruptedException 
	 * @throws ReasoningException 
	 * @throws SimpleParseException 
	 * @throws SemReasonerException 
	 */
	List<JSONObject> match() throws EDBException, IOException, SimpleParseException, ReasoningException, InterruptedException, SemReasonerException;
		
	
	/**
	 * Add a match to the case base. <br>
	 * matchBeforeFixes is the match without fixes in the messages. <br>
	 * matchAfterFixes is the match with fixes in the messages. <br>
	 * Returns false if matchAfterFixes is not a correct match.
	 * @param match
	 * @throws IOException 
	 * @throws SemReasonerException 
	 * @throws EDBException 
	 * @throws IDBException 
	 * @throws InterruptedException 
	 * @throws ReasoningException 
	 * @throws JSONException 
	 */
	boolean addToCaseBase(JSONObject matchBeforeFixes, JSONObject matchAfterFixes) throws IOException, SemReasonerException, EDBException, IDBException, JSONException, ReasoningException, InterruptedException;
	
	/**
	 * Returns true if are elements at all in the message pool which can match<br>
	 * @return true if there are messages which potentially could match
	 */
	boolean hasMatchableElements();
	
	/**
	 * Query for messages using OO-logic.
	 * @param ooquery, query in OO-Logic
	 * @return list of retrieved messages
	 * @throws IOException 
	 * @throws EDBException 
	 * @throws SemReasonerException 
	 * @throws InterruptedException 
	 * @throws ReasoningException 
	 */
	List<JSONObject> queryForMessages(String ooquery) throws EDBException, IOException, ReasoningException, InterruptedException, SemReasonerException;
	
	/**
	 * Query for all messages of a special message class. 
	 * @param messageClass, class of messages, e.g. SwiftMessage, MT202
	 * @return list of retrieved messages
	 * @throws IOException 
	 * @throws EDBException 
	 * @throws SemReasonerException 
	 * @throws InterruptedException 
	 * @throws ReasoningException 
	 */
	List<JSONObject> queryForMessagesInClass(String messageClass) throws EDBException, IOException, ReasoningException, InterruptedException, SemReasonerException;
		

}
