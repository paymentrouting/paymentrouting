package com.adesso.match;

import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.adesso.utils.FileUtils;
import com.semedy.reasoner.api.builtin.BuiltinProvider;
import com.semedy.reasoner.api.core.Configuration;
import com.semedy.reasoner.api.core.Configuration.StorageType;
import com.semedy.reasoner.api.core.Core;
import com.semedy.reasoner.api.core.DeductiveDatabase;
import com.semedy.reasoner.api.core.JSONDeductiveDatabase;
import com.semedy.reasoner.api.core.ResultBuffer;
import com.semedy.reasoner.api.core.ResultEnumerator;
import com.semedy.reasoner.api.edb.EDBException;
import com.semedy.reasoner.api.idb.IDBException;
import com.semedy.reasoner.api.idb.IntensionalDB;
import com.semedy.reasoner.api.logic.Literal;
import com.semedy.reasoner.api.logic.Rule;
import com.semedy.reasoner.api.parser.SimpleParseException;
import com.semedy.reasoner.api.reasoning.InterruptFlag;
import com.semedy.reasoner.api.reasoning.ReasoningException;
import com.semedy.reasoner.api.reasoning.ReasoningMonitor;
import com.semedy.reasoner.api.reasoning.SemReasonerException;
import com.semedy.reasoner.api.symboltable.SymbolTable;
import com.semedy.reasoner.api.symboltable.SymboltableException;
import com.semedy.reasoner.reasoning.processes.ReasoningRelation;
import com.semedy.reasoner.symboltable.encoder.JsonIdentifier;

public class MatchingImp implements Matching {
	/** the central reasoning core, also used as message pool */
	private Core _messagePool;
	/** the configuration */
	private Configuration _config;
	/** the case base */
	private Core _caseBase;
	/** random key generator */
	private static SecureRandom _random = new SecureRandom();
	/** the symbol table */
	private SymbolTable _symbols;

	public MatchingImp()
			throws SemReasonerException, IOException, EDBException, SymboltableException, InterruptedException {
		_config = new Configuration();
		_config.load();
		_messagePool = new Core(_config);
		_config.setEDBStorageType(StorageType.PERSISTENT);
		_config.setSymbolTableInMainMemory(false);
		_caseBase = new Core(_config);
		_symbols = _messagePool.getSymbolTable();
		loadImportDirectory();

	}

	private long generateIdNum() {
		return Math.abs(_random.nextLong());
	}

	private void select(File f, List<String> predfiles, List<String> rawfiles, List<String> jsonfiles) {
		String path = f.getAbsolutePath();
		if (path.endsWith("fct"))
			predfiles.add(f.getAbsolutePath());
		else if (path.endsWith("rls"))
			predfiles.add(f.getAbsolutePath());
		else if (path.endsWith("json"))
			jsonfiles.add(f.getAbsolutePath());
		else if (path.endsWith("raw"))
			rawfiles.add(f.getAbsolutePath());
	}

	/*
	 * load initial data and rule files
	 */
	private void loadImportDirectory() throws EDBException, IOException, SimpleParseException, SemReasonerException,
			InterruptedException, SymboltableException {
		List<String> predfiles = new ArrayList<>();
		List<String> rawfiles = new ArrayList<>();
		List<String> jsonfiles = new ArrayList<>();
		File f = new File(_config.getImportDir());
		if (f.isDirectory()) {
			File[] files = f.listFiles();
			for (int i = 0; i < files.length; i++)
				select(files[i], predfiles, rawfiles, jsonfiles);
		} else
			select(f, predfiles, rawfiles, jsonfiles);
		if (predfiles.size() + rawfiles.size() > 0) {
			DeductiveDatabase ddb = new DeductiveDatabase(_messagePool);
			if (predfiles.size() > 0)
				ddb.loadPredicateFiles(predfiles);
			if (rawfiles.size() > 0)
				ddb.loadRawFiles(rawfiles, false);
		}
		if (jsonfiles.size() > 0) {
			JSONDeductiveDatabase jdb = new JSONDeductiveDatabase(_messagePool);
			jdb.loadJson(jsonfiles);
		}
		String path = _config.getEDBDir() + "caseRules.rls";
		f = new File(path);
		if (f.exists()) {
			DeductiveDatabase ddb = new DeductiveDatabase(_caseBase);
			ddb.loadPredicateFiles(Collections.singletonList(_config.getEDBDir() + "caseRules.rls"));
		}
	}

	@Override
	public boolean addMessage(JSONObject message) throws SemReasonerException, EDBException, IOException {

		JSONDeductiveDatabase jddb = new JSONDeductiveDatabase(_messagePool);
		boolean result = false;
		try {
			jddb.transactionBegin();
			result = jddb.addJsonObject(message);
			jddb.transactionCommit();
		} catch (SemReasonerException | IOException | ReasoningException | InterruptedException e) {
			try {
				jddb.transactionRollback();
			} catch (SemReasonerException | IOException e1) {
				throw new SemReasonerException(e.getMessage());
			}
		}
		return result;
	}

	@Override
	public boolean removeMessage(String id) throws EDBException, IOException, SemReasonerException {
		JSONDeductiveDatabase jddb = new JSONDeductiveDatabase(_messagePool);
		boolean result = false;
		try {
			jddb.transactionBegin();
			result = jddb.removeJsonObject(id);
			jddb.transactionCommit();
		} catch (SemReasonerException | IOException | ReasoningException | InterruptedException e) {
			try {
				jddb.transactionRollback();
			} catch (SemReasonerException | IOException e1) {
				throw new SemReasonerException(e.getMessage());
			}
		}
		return result;
	}

	@Override
	public JSONObject getMessage(String id)
			throws ReasoningException, InterruptedException, IOException, SemReasonerException {
		JSONDeductiveDatabase jddb = new JSONDeductiveDatabase(_messagePool);
		JSONObject result = jddb.retrieveJsonObject(id);
		return result;
	}
	
	

	@Override
	public List<JSONObject> match() throws IOException, ReasoningException, InterruptedException, SemReasonerException {
		List<JSONObject> result = createMatchResult("?- match(?X,?Y,?R).", "direct");
		if (!result.isEmpty())
			return result;
		List<Rule> messagePoolRules = new ArrayList<>();
		messagePoolRules.addAll(_messagePool.getIntensionalDB().getRules());
		_messagePool.getIntensionalDB().clear();
		List<Rule> caseRules = _caseBase.getIntensionalDB().getRules();
		_messagePool.getIntensionalDB().addAllRules(caseRules);
		result = createCaseResult("?- matchCase1(?X,?Y,?Z).", "case", 1);
		_messagePool.getIntensionalDB().clear();
		_messagePool.getIntensionalDB().addAllRules(messagePoolRules);
		return result;
	}

	private List<JSONObject> createMatchResult(String query, String type)
			throws IOException, ReasoningException, InterruptedException, SemReasonerException {
		DeductiveDatabase ddb = new DeductiveDatabase(_messagePool);
		Map<JsonIdentifier, List<JsonIdentifier>> matches = new HashMap<>();
		try (ResultEnumerator enm = ddb.query(new InterruptFlag(), query, -1, null)) {
			enm.nextElement();
			ResultBuffer buf = enm.getBuffer();
			while (enm.hasMoreElements()) {
				enm.nextElement();
				JsonIdentifier messageId = (JsonIdentifier) buf.get("?X");
				List<JsonIdentifier> matchList = matches.get(messageId);
				if (matchList == null) {
					matchList = new ArrayList<>();
					matches.put(messageId, matchList);
				}
				matchList.add((JsonIdentifier) buf.get("?Y"));
			}
		}
		JSONDeductiveDatabase jddb = new JSONDeductiveDatabase(_messagePool);
		List<JSONObject> result = new ArrayList<>();
		for (Entry<JsonIdentifier, List<JsonIdentifier>> E : matches.entrySet()) {
			JSONObject json = new JSONObject();
			JsonIdentifier jsonid = (JsonIdentifier) E.getKey();
			String s = jsonid.getValue().toString();
			JSONObject message = jddb.retrieveJsonObject(s);
			JSONArray array = new JSONArray();
			array.put(message);
			json.put("messages", array);
			List<String> matchids = new ArrayList<>();
			for (JsonIdentifier id : E.getValue())
				matchids.add(id.getValue().toString());
			List<JSONObject> matched = jddb.retrieveJsonObjects(matchids);
			json.put("matchTo", matched);
			json.put("type", type);
			result.add(json);
		}
		return result;
	}
	
	private List<JSONObject> createCaseResult(String query, String type, int deviation)
			throws IOException, ReasoningException, InterruptedException, SemReasonerException {
		DeductiveDatabase ddb = new DeductiveDatabase(_messagePool);
		Map<JsonIdentifier, List<JsonIdentifier>> matches = new HashMap<>();
		Map<JsonIdentifier, String> cases = new HashMap<>();
		try (ResultEnumerator enm = ddb.query(new InterruptFlag(), query, -1, null)) {
			enm.nextElement();
			ResultBuffer buf = enm.getBuffer();
			while (enm.hasMoreElements()) {
				enm.nextElement();
				JsonIdentifier messageId = (JsonIdentifier) buf.get("?X");
				List<JsonIdentifier> matchList = matches.get(messageId);
				if (matchList == null) {
					matchList = new ArrayList<>();
					matches.put(messageId, matchList);
				}
				matchList.add((JsonIdentifier) buf.get("?Y"));
				cases.put(messageId, buf.get("?Z").toString());
			}
		}
		JSONDeductiveDatabase jddb = new JSONDeductiveDatabase(_messagePool);
		JSONDeductiveDatabase cjddb = new JSONDeductiveDatabase(_caseBase);
		List<JSONObject> result = new ArrayList<>();
		for (Entry<JsonIdentifier, List<JsonIdentifier>> E : matches.entrySet()) {
			JSONObject json = new JSONObject();
			JsonIdentifier jsonid = (JsonIdentifier) E.getKey();
			String s = jsonid.getValue().toString();
			JSONObject message = jddb.retrieveJsonObject(s);
			JSONArray array = new JSONArray();
			array.put(message);
			json.put("messages", array);
			List<String> matchids = new ArrayList<>();
			for (JsonIdentifier id : E.getValue())
				matchids.add(id.getValue().toString());
			List<JSONObject> matched = jddb.retrieveJsonObjects(matchids);
			String caseId = cases.get(E.getKey());
			JSONObject cs = cjddb.retrieveJsonObject(caseId);
			json.put("matchTo", matched);
			json.put("type", type);
			json.put("deviation", deviation);
			json.put("case", cs);
			System.out.println(json.toString(4));
			result.add(json);
		}
		return result;
	}

	private Rule createCaseRule(String caseId, Rule r, int deviation) throws SymboltableException, IOException, IDBException {
		Literal[] bodies = new Literal[r.getBodyLength()];
		for (int i = 0; i < bodies.length; i++)
			bodies[i] = r.getBody(i).clone();
		Long caseIdCode = _symbols.encode(caseId);
		Literal head = new Literal("matchCase"+deviation, true, new Object[] {r.getHead().getArgument(0), r.getHead().getArgument(1),caseIdCode });
		Rule result = new Rule(r.getIdentifier(), head, bodies);
		return result;
	}

	@Override
	public boolean addToCaseBase(JSONObject matchBeforeFixes, JSONObject matchAfterFixes)
			throws IOException, SemReasonerException, JSONException, ReasoningException, InterruptedException {
		Object[] result = relaxRules(matchBeforeFixes, matchAfterFixes);
		if (result == null)
			return false;

		if (_caseBase.getIntensionalDB().getRuleById(((Rule) result[1]).getDocumentation()) == null) {
			String id = Long.valueOf(generateIdNum()).toString();
			Rule caseRule = createCaseRule("case_"+id, (Rule) result[1],(Integer)result[2]);
			_caseBase.getIntensionalDB().addRule(caseRule);
			_caseBase.getIntensionalDB().storeInRuleFile(_config.getEDBDir() + "caseRules.rls", true,
					_symbols);
			String caseId = "case_"+id;
			JSONObject thisCase = new JSONObject();
			thisCase.put("matchBeforeFixes", matchBeforeFixes);
			thisCase.put("matchAfterFixes", matchAfterFixes);
			thisCase.put("@id", caseId);
			thisCase.put("@type", "Case");

			JSONDeductiveDatabase jddb = new JSONDeductiveDatabase(_caseBase);
			try {
				jddb.transactionBegin();
				jddb.addJsonObject(thisCase);
			} catch (Exception e) {
				jddb.transactionRollback();
			} finally {
				jddb.transactionCommit();
			}
		}

		return true;
	}

	/*
	 * relax rules, i.e. delete rule bodies until most similar case is found
	 */
	private Object[] relaxRules(JSONObject matchBeforeFixes, JSONObject matchAfterFixes)
			throws IOException, SemReasonerException, JSONException, ReasoningException, InterruptedException {
		Configuration config = new Configuration();
		config.load();
		config.setEDBStorageType(StorageType.MEMORY);
		config.setSymbolTableInMainMemory(true);
		try (Core core = new Core(config)) {
			core.setSymbolTable(_symbols);
			JSONDeductiveDatabase jddb = new JSONDeductiveDatabase(core);
			core.getIntensionalDB().addAllRules(_messagePool.getIntensionalDB().getRules());
			jddb.transactionBegin();
			jddb.addJsonObjects(matchAfterFixes.getJSONArray("messages"), "");
			jddb.addJsonObjects(matchAfterFixes.getJSONArray("matchTo"), "");
			List<String> ruleIdentifiers = new ArrayList<>();
			DeductiveDatabase ddb = jddb.getDeductiveDatabase();
			try (ResultEnumerator enm = ddb.query(new InterruptFlag(), "?- match(?X,?Y,?Z).", -1, null)) {
				enm.nextElement();
				ResultBuffer buf = enm.getBuffer();
				if (!enm.hasMoreElements())
					return null;
				while (enm.hasMoreElements()) {
					enm.nextElement();
					String ruleId = buf.get("?Z").toString();
					ruleIdentifiers.add(ruleId);
				}
			}
			jddb.transactionRollback();

			jddb.transactionBegin();
			jddb.addJsonObjects(matchBeforeFixes.getJSONArray("messages"), "");
			jddb.addJsonObjects(matchBeforeFixes.getJSONArray("matchTo"), "");
			Rule ruleToRemove = null;
			Rule resultRule = null;
			int count = Integer.MAX_VALUE;
			IntensionalDB idb = core.getIntensionalDB();
			for (String ruleId : ruleIdentifiers) {
				Rule r = idb.getRuleById(ruleId);
				Object[] result = relaxRule(ddb, core, ruleId);
				if ((Integer) result[1] < count) {
					resultRule = idb.getRuleById((String) result[0]);
					ruleToRemove = r;
					count = (Integer) result[1];
				}
				idb.removeRuleById((String) result[0]);
				idb.addRule(r);
			}
			jddb.transactionRollback();

			return new Object[] { ruleToRemove, resultRule, count };
		}
	}

	private Rule removeBodyAtIndex(Rule rule, int index) throws IDBException {
		Literal[] bodies = new Literal[rule.getBodyLength() - 1];
		for (int i = 0; i < index; i++)
			bodies[i] = rule.getBody(i).clone();
		for (int i = index + 1; i < rule.getBodyLength(); i++)
			bodies[i - 1] = rule.getBody(i).clone();
		Rule newrule = new Rule(rule.getIdentifier() + "_" + index, rule.getHead().clone(), bodies);
		return newrule;
	}

	private Object[] relaxRule(DeductiveDatabase ddb, Core core, String ruleIdentifier)
			throws IOException, SimpleParseException, ReasoningException, InterruptedException {
		LearnMonitor monitor = new LearnMonitor(ruleIdentifier);
		boolean relaxed = false;
		int count = 0;
		do {
			try (ResultEnumerator enm = ddb.query(new InterruptFlag(), "?- match(?X,?Y,?Z).", -1, monitor)) {
				enm.nextElement();
				if (!enm.hasMoreElements()) {
					// relax rule
					Rule rule = core.getIntensionalDB().getRuleById(ruleIdentifier);
					Literal firstFalseLiteral = monitor.getFirstFalseLiteral();
					if (firstFalseLiteral != null) {
						int index = -1;
						for (int i = 0; i < rule.getBodyLength(); i++)
							if (rule.getBody(i) == firstFalseLiteral)
								index = i;
						if (index != -1) {
							Rule newrule = removeBodyAtIndex(rule, index);
							core.getIntensionalDB().removeRuleById(rule.getIdentifier());
							core.getIntensionalDB().addRule(newrule);
							ruleIdentifier = newrule.getIdentifier();
							monitor.clear();
							monitor.setRuleIdentifier(ruleIdentifier);
						}
					}
					count++;
				} else {
					relaxed = true;
					System.out.println();
					System.out.println(
							core.getIntensionalDB().getRuleById(ruleIdentifier).toString(core.getSymbolTable()));
					return new Object[] { ruleIdentifier, Integer.valueOf(count) };
				}
			}
		} while (!relaxed);
		return null;
	}

	@Override
	public boolean hasMatchableElements() {
		return false;
	}

	@Override
	public List<JSONObject> queryForMessages(String ooquery)
			throws IOException, ReasoningException, InterruptedException, SemReasonerException {
		JSONDeductiveDatabase jddb = new JSONDeductiveDatabase(_messagePool);
		return jddb.retrieveJsonObjects(ooquery);
	}

	@Override
	public List<JSONObject> queryForMessagesInClass(String messageClass)
			throws IOException, ReasoningException, InterruptedException, SemReasonerException {
		JSONDeductiveDatabase jddb = new JSONDeductiveDatabase(_messagePool);
		return jddb.retrieveJsonObjects("?- ?Id:" + messageClass);
	}

	class LearnMonitor implements ReasoningMonitor {
		private Literal _firstLiteral = null;
		private String _ruleId;

		public LearnMonitor(String ruleId) {
			_ruleId = ruleId;
		}

		@Override
		public void postBuiltinExecution(Rule arg0, Literal arg1, String arg2, String arg3, ReasoningRelation[] arg4,
				long arg5, ReasoningRelation arg6, String arg7) throws IOException {
			// TODO Auto-generated method stub

		}

		@Override
		public void postEvaluation(String arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void postJoin(Rule rule, Literal literal, String arg3, String arg4, ReasoningRelation[] arg5,
				ReasoningRelation[] arg6, long numberOfResults, ReasoningRelation arg8, String arg9, String arg10)
				throws IOException {
			if (numberOfResults == 0 && rule.getIdentifier().startsWith(_ruleId) && _firstLiteral == null)
				_firstLiteral = literal.getOriginal();
		}

		@Override
		public void postRuleEvaluation(Rule arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void preBuiltinExecution(Rule arg0, Literal arg1, String arg2, String arg3, ReasoningRelation[] arg4,
				String arg5) throws IOException {
			// TODO Auto-generated method stub

		}

		@Override
		public void preEvalution(String arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void preFreeMemory(String arg0, long arg1) {
			// TODO Auto-generated method stub

		}

		@Override
		public void preJoin(Rule arg0, Literal arg1, String arg2, String arg3, ReasoningRelation[] arg4,
				ReasoningRelation[] arg5, String arg6) throws IOException {
			// TODO Auto-generated method stub

		}

		@Override
		public void preMemoryAllocation(String arg0, long arg1) {
			// TODO Auto-generated method stub

		}

		@Override
		public void preRuleEvaluation(Rule arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void setBuiltins(BuiltinProvider arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void setIntensionalDB(IntensionalDB arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void setReasoningSymbolTable(SymbolTable arg0) {
			// TODO Auto-generated method stub

		}

		public Literal getFirstFalseLiteral() {
			return _firstLiteral.getOriginal();
		}

		public void clear() {
			_firstLiteral = null;
		}

		public void setRuleIdentifier(String ruleId) {
			_ruleId = ruleId;
		}

	}

}
