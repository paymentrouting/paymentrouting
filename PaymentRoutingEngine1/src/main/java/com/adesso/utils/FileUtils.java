/*****************************************************************************
 * Copyright (c) 2013 J. Angele.
 *
 * All rights reserved.
 *
 * Created on: 25.11.2013
 * Created by: J.Angele
 *****************************************************************************/

package com.adesso.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * utils for files
 * 
 * @author angele
 * 
 */
public class FileUtils {

	static public void cleanDirectory(File f) {
		if (f.isDirectory()) {
			String[] files = f.list();
			for (String filename : files) {
				File f1 = new File(f.getAbsolutePath() + "/" + filename);
				if (f1.isDirectory()) {
					FileUtils.cleanDirectory(f1);
					f1.delete();
				} else {
					f1.delete();
				}
			}
		}
	}

	static public void cleanDirectoryFlat(File f) {
		if (f.isDirectory()) {
			String[] files = f.list();
			for (String filename : files) {
				File f1 = new File(f.getAbsolutePath() + "/" + filename);
				if (!f1.isDirectory())
					f1.delete();
			}
		}
	}

	public static void copyFile(File source, File dest) throws IOException {
		Path sp = Paths.get(source.getAbsolutePath());
		Path dp = Paths.get(dest.getAbsolutePath());
		Files.copy(sp, dp);
	}

	public static void copyFile(InputStream source, File dest) throws IOException {
		Path dp = Paths.get(dest.getAbsolutePath());
		Files.copy(source, dp);
	}

	static public void copyDirectoryFlat(File source, File dest) throws IOException {
		if (source.isDirectory()) {
			String[] files = source.list();
			for (String filename : files) {
				File f1 = new File(source.getAbsolutePath() + "/" + filename);
				File f2 = new File(dest.getAbsolutePath() + "/" + filename);
				copyFile(f1, f2);
			}
		}
	}

	public static String readFile(String fileName) throws IOException {
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			return readFile(br);
		}
	}
	
	public static void writeFile(String fileName, String s) throws IOException {
		try (BufferedWriter br = new BufferedWriter(new FileWriter(fileName))) {
			br.write(s);
			br.flush();
		}
	}

	public static String readFile(BufferedReader br) throws IOException {
		StringBuilder sb = new StringBuilder();
		String line = br.readLine();

		while (line != null) {
			sb.append(line);
			sb.append("\n");
			line = br.readLine();
		}
		return sb.toString();
	}
}
