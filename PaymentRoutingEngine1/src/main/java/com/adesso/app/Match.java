package com.adesso.app;

import java.io.IOException;
import java.util.List;

import org.json.JSONObject;

import com.adesso.match.Matching;
import com.adesso.match.MatchingImp;
import com.adesso.match.SWIFTReader;
import com.semedy.reasoner.api.reasoning.ReasoningException;
import com.semedy.reasoner.api.reasoning.SemReasonerException;

public class Match {
	
	public static void main(String[] args) throws IOException, SemReasonerException, InterruptedException, ReasoningException {
		if (args.length != 1)
			System.out.println("usage: <message directory>");
		else {
			SWIFTReader reader = new SWIFTReader();
			List<JSONObject> messages = reader.read(args[0]);
			if (messages.size() > 0) {
				Matching knowledge = new MatchingImp();
				for(JSONObject message: messages)
					knowledge.addMessage(message);				
				List<JSONObject> result = knowledge.match();
				if (result.size() > 0)
					System.out.println("matching results");
				else
					System.out.println("no matching results found");
				for(JSONObject json: result)
					System.out.println(json.toString(4));
			}
		}
	}
}
