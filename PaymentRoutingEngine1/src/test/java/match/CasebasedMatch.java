
package match;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import com.adesso.match.Matching;
import com.adesso.match.MatchingImp;
import com.adesso.utils.FileUtils;
import com.semedy.reasoner.api.core.Configuration;

public class CasebasedMatch {

	@Test
	public void testAddToCasebase() throws Exception {
		// Scenario 1
		// add a case to the case base
		// relax rule
		Configuration config = new Configuration();
		config.load();
		FileUtils.cleanDirectory(new File(config.getEDBDir()));
		String nomatch = FileUtils.readFile("./src/test/resources/casebasedMatch/nomatch.json");
		String match = FileUtils.readFile("./src/test/resources/casebasedMatch/match.json");
		JSONArray nomatchjson = new JSONArray(nomatch);
		JSONArray matchjson = new JSONArray(match);
		Matching matcher = new MatchingImp();
		boolean found =  matcher.addToCaseBase(nomatchjson.getJSONObject(0), matchjson.getJSONObject(0));
		assertTrue(found);
		FileUtils.cleanDirectory(new File(config.getEDBDir()));
	}
	
	@Test
	public void testCasebasedMatch() throws Exception {
		// Scenario 4
		// add messages which do not match
		Configuration config = new Configuration();
		config.load();
		FileUtils.cleanDirectory(new File(config.getEDBDir()));
		String nomatch = FileUtils.readFile("./src/test/resources/casebasedMatch/nomatch.json");
		String match = FileUtils.readFile("./src/test/resources/casebasedMatch/match.json");
		JSONArray nomatchjson = new JSONArray(nomatch);
		JSONArray matchjson = new JSONArray(match);
		Matching matcher = new MatchingImp();
		// add to case base
		boolean found =  matcher.addToCaseBase(nomatchjson.getJSONObject(0), matchjson.getJSONObject(0));
		assertTrue(found);
		
		// add messages to message pool
		JSONArray arr = nomatchjson.getJSONObject(0).getJSONArray("messages");
		for(int i = 0; i < arr.length(); i++) {
			JSONObject message = arr.getJSONObject(i);
			matcher.addMessage(message);
		}
		 arr = nomatchjson.getJSONObject(0).getJSONArray("matchTo");
		for(int i = 0; i < arr.length(); i++) {
			JSONObject message = arr.getJSONObject(i);
			matcher.addMessage(message);
		}
		
		// try to match, finally the cbr matcher finds the match
		List<JSONObject> matches = matcher.match();
		assertTrue(matches.size()>0);
		FileUtils.cleanDirectory(new File(config.getEDBDir()));
		
	}

}