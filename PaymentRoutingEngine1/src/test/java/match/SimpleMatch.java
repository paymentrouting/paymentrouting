
package match;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.json.JSONObject;
import org.junit.Ignore;
import org.junit.Test;

import com.adesso.match.Matching;
import com.adesso.match.MatchingImp;
import com.adesso.match.SWIFTReader;

public class SimpleMatch {

	@Test
	public void testCase1() throws Exception {
		SWIFTReader reader = new SWIFTReader();
		List<JSONObject> messages = reader.read("src/test/resources/messages/case1");
		assertTrue(messages.size() > 0);
		Matching knowledge = new MatchingImp();
		for (JSONObject message : messages)
			knowledge.addMessage(message);
		List<JSONObject> match = knowledge.match();
		assertTrue(match.size()>0);
	}
	
	@Ignore
	@Test
	public void testCase2() throws Exception {
		SWIFTReader reader = new SWIFTReader();
		List<JSONObject> messages = reader.read("src/test/resources/messages/case2");
		assertTrue(messages.size() > 0);
		Matching knowledge = new MatchingImp();
		for (JSONObject message : messages)
			knowledge.addMessage(message);
		List<JSONObject> match = knowledge.match();
		assertTrue(match.size()>0);
	}

}